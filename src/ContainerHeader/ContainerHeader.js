import "./ContainerHeader.css";
import BackgroundColor from "./BackgroundColor";
import HeaderSearch from "./HeaderSearch";
function ContainerHeader() {
  return (
    <div className="container__header">
      <HeaderSearch />
      <BackgroundColor />
    </div>
  );
}
export default ContainerHeader;
