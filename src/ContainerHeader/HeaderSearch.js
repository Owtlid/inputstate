import "./HeaderSearch.css";
import { useState } from "react";
function HeaderSearch() {
  const [inputValue, setInputValue] = useState("");
  const changeInput = (event) => {
    setInputValue(event.target.value);
    console.log(event.target.value);
  };
  const search = () => {
    const arr = [
      "Mary",
      "Patricia",
      "Linda",
      "Barbara",
      "Elizabeth",
      "Jennifer",
      "Maria",
      "Susan",
      "Margaret",
      "Ddrothy",
    ];
    if (arr.includes(inputValue)) {
      alert(`index: ${arr.indexOf(inputValue)}`);
    }else{
      alert("NOPE");
    }
  };
  return (
    <>
      <input
        className="container__header-text"
        type="text"
        placeholder="...."
        onChange={changeInput}
      />
      <button className="container__header-button" onClick={search}>
        search
      </button>
    </>
  );
}
export default HeaderSearch;
