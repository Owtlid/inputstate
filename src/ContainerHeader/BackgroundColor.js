import "./BackgroundColor.css";
import { useState } from "react";
function BackgroundColor() {
  const [defaultColor, setDefaultColor] = useState("#282C34");
  const changeBColor = (event) => {
    setDefaultColor(event.target.value);
    document.body.style.backgroundColor = defaultColor;
  };
  return (
    <input className="container__color" type="color" onChange={changeBColor} />
  );
}
export default BackgroundColor;
