import "./App.css";
import ContainerHeader from "./ContainerHeader/ContainerHeader";
import ContainerBody from "./ContainerBody/ContainerBody";

function App() {
  return (
    <div className="container">
      <ContainerHeader />
      <ContainerBody selected />
    </div>
  );
}

export default App;
