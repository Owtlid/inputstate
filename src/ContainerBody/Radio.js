import "./Radio.css";
import { useState } from "react";
function Radio() {
  const [value, setValue] = useState("#fff");
  const radioChange = (event) => {
    setValue(event.target.value);
  };
  const changeBooleanValue = () => {
    setValue("#fff");
  };
  return (
    <div className="container__radio" style={{ color: `${value}` }}>
      <p className="container__text">
        Lorem Ipsum is simply dummy text of the printing and typesetting
        industry. Lorem Ipsum has been the industry's standard dummy text ever
        since the 1500s, when an unknown printer took a galley of type and
        scrambled it to make a type specimen book.
      </p>
      <input type="radio" name="color" value="red" onChange={radioChange} />
      <input type="radio" name="color" value="green" onChange={radioChange} />
      <input type="radio" name="color" value="blue" onChange={radioChange} />
      <button className="container__radio-button" onClick={changeBooleanValue}>
        reset
      </button>
    </div>
  );
}
export default Radio;
