import "./ContainerBody.css";
import { useState } from "react";
import Radio from "./Radio";
function ContainerBody({ selected }) {
  const [checkboxS, setCheckboxS] = useState(selected);
  const checkbox = (event) => {
    setCheckboxS(event.target.checked);
  };
  return (
    <div className="container__body">
      <input
        className={
          "container__checkbox" + [checkboxS ? " container__checkbox--big" : ""]
        }
        type="checkbox"
        onChange={checkbox}
        checked={checkboxS}
      />
      <Radio />
    </div>
  );
}
export default ContainerBody;
